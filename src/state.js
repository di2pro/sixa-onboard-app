export const SCREENS = {
  WELCOME: 'welcome',
  NAME: 'name',
  SITE: 'site',
  CHOICE: 'choice',
  PICKER: 'picker',
  INSPIRATION: 'inspiration',
  CONFIRM: 'confirm',
  BYE: 'bye',
};

export const MSG_TYPES = {
  DEFAULT: 'default',
  ACTION: 'action',
  INPUT: 'input',
  PICKER: 'picker',
  ACTION_TRANSPARENT: 'action--transparent',
  LINK: 'link',
  INSPIRATION: 'inspiration',
};

export const DATA = {
  USER_NAME: 'user-name',
  WEB_SITE: 'web-site',
  USER_EMAIL: 'e-mail',
  USER_PHONE: 'phone',
  SEO_1: 'seo-1',
  SEO_2: 'seo-2',
  SEO_3: 'seo-3',
  SEO_4: 'seo-4',

  SEA_1: 'sea-1',
  SEA_2: 'sea-2',
  SEA_3: 'sea-3',

  ANALYTICS_1: 'eventData-1',
  ANALYTICS_2: 'eventData-2',
  ANALYTICS_3: 'eventData-3',
  ANALYTICS_4: 'eventData-4',

  SMM_1: 'smm-1',
  SMM_2: 'smm-2',
  SMM_3: 'smm-3',

  CRO_1: 'cro-1',
  CRO_2: 'cro-2',
  CRO_3: 'cro-3',
  CRO_4: 'cro-4',

  WEB_DEV_1: 'web-dev-1',
  WEB_DEV_2: 'web-dev-2',

  INSPIRED_BY: 'inspired-by',
  LAST_PICKED: 'last-picked',
  VALIDATION_ERROR: 'validation-error',
};

export const data = {
  [DATA.USER_NAME]: '',
  [DATA.WEB_SITE]: '',
  [DATA.USER_PHONE]: '',
  [DATA.USER_EMAIL]: '',

  [DATA.SEO_1]: false,
  [DATA.SEO_2]: false,
  [DATA.SEO_3]: false,
  [DATA.SEO_4]: false,

  [DATA.SEA_1]: false,
  [DATA.SEA_2]: false,
  [DATA.SEA_3]: false,

  [DATA.ANALYTICS_1]: false,
  [DATA.ANALYTICS_2]: false,
  [DATA.ANALYTICS_3]: false,
  [DATA.ANALYTICS_4]: false,

  [DATA.SMM_1]: false,
  [DATA.SMM_2]: false,
  [DATA.SMM_3]: false,

  [DATA.CRO_1]: false,
  [DATA.CRO_2]: false,
  [DATA.CRO_3]: false,
  [DATA.CRO_4]: false,

  [DATA.WEB_DEV_1]: false,
  [DATA.WEB_DEV_2]: false,

  [DATA.INSPIRED_BY]: '',
  [DATA.LAST_PICKED]: '',
  [DATA.VALIDATION_ERROR]: '',
};

export const steps = {
  [SCREENS.WELCOME]: {
    isBotHeadSmall: false,
    messages:
    [
      {
        type: MSG_TYPES.DEFAULT,
        text: `Hallo ich bin Tom, dein persönlicher Berater!`,
        animationTimeout: {
          enter: 500,
          exit: 0,
        },
      },
      {
        type: MSG_TYPES.DEFAULT,
        text:
          `Um dich auf deinem Weg zum Ziel zu führen,
          stelle ich dir nun ein paar wichtige Fragen.`,
        animationTimeout: {
          enter: 800,
          exit: 0,
        },
      },
      {
        type: MSG_TYPES.ACTION,
        text: `Okey!`,
        linkTo: SCREENS.NAME,
        eventData: {
          action: 'Hello',
        },
        animationTimeout: {
          enter: 1300,
          exit: 0,
        },
      },
      {
        type: MSG_TYPES.LINK,
        text: `Zurück zur Startseite`,
        linkTo: 'https://sixa.ch/',
        isExternal: true,
        animationTimeout: {
          enter: 1500,
          exit: 0,
        },
      },
    ],
  },
  [SCREENS.NAME]: {
    messages:
    [
      {
        type: MSG_TYPES.DEFAULT,
        text: `Darf ich deinen Namen erfahren?`,
        animationTimeout: {
          enter: 500,
          exit: 0,
        },
      },
      {
        type: MSG_TYPES.INPUT,
        dataKey: DATA.USER_NAME,
        placeholder: 'Max Muster',
        animationTimeout: {
          enter: 800,
          exit: 0,
        },
      },
      {
        type: MSG_TYPES.ACTION,
        text: `Weiter`,
        linkTo: SCREENS.SITE,
        eventData: {
          action: 'Name',
        },
        animationTimeout: {
          enter: 1300,
          exit: 0,
        },
      },
      {
        type: MSG_TYPES.LINK,
        text: `Zurück`,
        linkTo: SCREENS.WELCOME,
        animationTimeout: {
          enter: 1500,
          exit: 0,
        },
      },
    ],
    validate({ [DATA.USER_NAME]:userName }) {
      return !userName.length ? DATA.USER_NAME : '';
    }
  },
  [SCREENS.SITE]: {
    messages:
    [
      {
        type: MSG_TYPES.DEFAULT,
        text: `Für welche Webseite möchtest du Massnahmen durchführen?`,
        animationTimeout: {
          enter: 500,
          exit: 0,
        },
      },
      {
        type: MSG_TYPES.INPUT,
        dataKey: DATA.WEB_SITE,
        placeholder: 'https://digitec.ch/',
        animationTimeout: {
          enter: 800,
          exit: 0,
        },
      },
      {
        type: MSG_TYPES.ACTION,
        text: `Weiter`,
        linkTo: SCREENS.CHOICE,
        eventData: {
          action: 'Site',
        },
        animationTimeout: {
          enter: 1300,
          exit: 0,
        },
      },
      {
        type: MSG_TYPES.LINK,
        text: `Zurück`,
        linkTo: SCREENS.NAME,
        animationTimeout: {
          enter: 1500,
          exit: 0,
        },
      },
    ],
    validate({ [DATA.WEB_SITE]:userSite }) {
      const webSiteRegEx = /^((https?):\/\/)?(www.)?[a-z0-9-]+\.[a-z]+(\/[a-zA-Z0-9#]*\/?)*$/;

      return !userSite.length || !webSiteRegEx.test(userSite) ? DATA.WEB_SITE : '';
    }
  },
  [SCREENS.CHOICE]: {
    messages:
    [
      {
        type: MSG_TYPES.DEFAULT,
        text:
          `Weisst du bereits bescheid,
          welche Dienstleistungen dich interessieren
          oder möchtest du dich von den Erfolgsgeschichten
          unserer Kunden inspirieren lassen?`,
        animationTimeout: {
          enter: 500,
          exit: 0,
        },
      },
      {
        type: MSG_TYPES.ACTION_TRANSPARENT,
        text: `Ich weiss bereits was ich benötige.`,
        linkTo: SCREENS.PICKER,
        eventData: {
          action: 'Service or stories',
          label: 'I already know what I need',
        },
        animationTimeout: {
          enter: 800,
          exit: 0,
        },
      },
      {
        type: MSG_TYPES.ACTION_TRANSPARENT,
        text: `Ich möchte mich inspirieren lassen.`,
        linkTo: SCREENS.INSPIRATION,
        eventData: {
          action: 'Service or stories',
          label: 'I want to be inspired',
        },
        animationTimeout: {
          enter: 1000,
          exit: 0,
        },
      },
      {
        type: MSG_TYPES.LINK,
        text: `Zurück`,
        linkTo: SCREENS.SITE,
        animationTimeout: {
          enter: 1300,
          exit: 0,
        },
      },
    ],
  },
  [SCREENS.PICKER]: {
    messages:
    [
      {
        type: MSG_TYPES.DEFAULT,
        text: `Wähle die Dienstleistungen aus die du für %${DATA.WEB_SITE}% benötigst.`,
        animationTimeout: {
          enter: 500,
          exit: 0,
        },
      },
      {
        type: MSG_TYPES.DEFAULT,
        text: `Aufbauend auf deinem Feedback werden wir dir passende Massnahmen vorschlagen.`,
        animationTimeout: {
          enter: 800,
          exit: 0,
        },
      },
      {
        type: MSG_TYPES.PICKER,
        items:
        [
          {
            title: 'Suchmaschinenoptimierung',
            description:
              `Massgeschneiderte Optimierungen und Linkaufbau in 
              Kombination mit unschlagbaren Analysen.`,
            options: [
              {
                label: 'Ich möchte meine Rankings verbessern',
                dataKey: DATA.SEO_1
              },
              {
                label: 'Ich möchte einen SEO-Check durchführen',
                dataKey: DATA.SEO_2
              },
              {
                label: 'Ich möchte mehr Kunden über Google erhalten (organisch)',
                dataKey: DATA.SEO_3
              },
              {
                label: 'Ich möchte mein Linkprofil verbessern',
                dataKey: DATA.SEO_4
              },
            ],
            animationTimeout: {
              enter: 1100,
              exit: 0,
            },
          },
          {
            title: 'Suchmaschinenwerbung',
            description:
              `Skalierbare und performante Kampagnen um 
              Kunden richtig anzusprechen.`,
            options: [
              {
                label: 'Ich möchte mehr Kunden über Google Ads erhalten',
                dataKey: DATA.SEA_1,
              },
              {
                label: 'Ich möchte meine Google Ads Kampagne nicht mehr selbst verwalten',
                dataKey: DATA.SEA_2,
              },
              {
                label: 'Ich möchte die Performance meiner Google Ads Kampagne verbessern',
                dataKey: DATA.SEA_3,
              },
            ],
            animationTimeout: {
              enter: 1400,
              exit: 0,
            },
          },
          {
            title: 'Analytics',
            description:
              `Kompetente Beratung und Einrichtung der mondernsten Analytic-Tools 
              zur erfolgreichen Erfassung relevanter Metriken.`,
            options: [
              {
                label: 'Ich möchte alle Metriken meiner Webseite erfassen',
                dataKey: DATA.ANALYTICS_1,
              },
              {
                label: 'Ich möchte die Konversionsrate meiner Webseite messen',
                dataKey: DATA.ANALYTICS_2,
              },
              {
                label: 'Ich möchte wissen, wo ich Kunden verliere',
                dataKey: DATA.ANALYTICS_3,
              },
              {
                label: 'Ich möchte wissen, wo sich Kunden aufhalten',
                dataKey: DATA.ANALYTICS_4,
              },
            ],
            animationTimeout: {
              enter: 1700,
              exit: 0,
            },
          },
          {
            title: 'Konversionsoptimierung',
            description:
              `Verborgenes Potential nutzen und aus Besuchern Kunden machen.`,
            options: [
              {
                label: 'Ich möchte die Performance meiner Webseite verbessern',
                dataKey: DATA.CRO_1,
              },
              {
                label: 'Ich möchte meine Konversionsrate erhöhen',
                dataKey: DATA.CRO_2,
              },
              {
                label: 'Ich möchte meine Absprungrate verringern',
                dataKey: DATA.CRO_3,
              },
              {
                label: 'Ich möchte meine Sitzungsdauer verlängern',
                dataKey: DATA.CRO_4,
              },
            ],
            animationTimeout: {
              enter: 2000,
              exit: 0,
            },
          },
          {
            title: 'Social Media Marketing',
            description:
              `Mit individuellen Kampagnen Interesse wecken und neue Zielgruppen erreichen.`,
            options: [
              {
                label: 'Ich möchte mehr Kunden über Facebook, Twitter oder LinkedIn erhalten',
                dataKey: DATA.SMM_1,
              },
              {
                label: 'Ich möchte meine Facebook, Twitter oder LinkedIn Ads Kampagne nicht mehr selbst verwalten',
                dataKey: DATA.SMM_2,
              },
              {
                label: 'Ich möchte die Performance meiner Facebook, Twitter oder LinkedIn Ads Kampagne Kampagne verbessern',
                dataKey: DATA.SMM_3,
              },
            ],
            animationTimeout: {
              enter: 2300,
              exit: 0,
            },
          },
          {
            title: 'Web Development',
            description:
              `Entwicklung von massgeschneiderten Webseiten und Webapplikationen.`,
            options: [
              {
                label: 'Ich möchte eine neue Webseite',
                dataKey: DATA.WEB_DEV_1,
              },
              {
                label: 'Ich möchte meine Webseite überarbeiten',
                dataKey: DATA.WEB_DEV_2,
              },
            ],
            animationTimeout: {
              enter: 2600,
              exit: 0,
            },
          },
        ]
      },
      {
        type: MSG_TYPES.ACTION,
        text: `Weiter`,
        linkTo: SCREENS.CONFIRM,
        eventData: {
          action: 'Choice',
          label: DATA.LAST_PICKED,
        },
        animationTimeout: {
          enter: 3000,
          exit: 0,
        },
      },
      {
        type: MSG_TYPES.LINK,
        text: `Zurück`,
        linkTo: SCREENS.CHOICE,
        animationTimeout: {
          enter: 3100,
          exit: 0,
        },
      },
    ]
  },
  [SCREENS.INSPIRATION]: {
    messages:
    [
      {
        type: MSG_TYPES.DEFAULT,
        text: `Wähle einen vordefinierten Pfad unserer Kunden.`,
        animationTimeout: {
          enter: 500,
          exit: 0,
        }
      },
      {
        type: MSG_TYPES.INSPIRATION,
        cards:
        [
          {
            title: `Lebensmittel`,
            type: `E-Commerce B2C`,
            description:
              `Implementierung einer neuen Linkstruktur und 
              Metadaten für über 3.000 Produktseiten. Gefolgt 
              auf darauf ausgerichtete Suchmaschinenoptimierung 
              mit entsprechender Überwachung bestimmter Metriken.`,
            image: `lebensmittel`,
            linkTo: SCREENS.CONFIRM,
            dataKey: DATA.INSPIRED_BY,
            eventData: {
              action: 'Choice',
              label: 'Lebensmittel',
            },
            animationTimeout: {
              enter: 1000,
              exit: 0,
            }
          },
          {
            title: `Genussmittel`,
            type: `E-Commerce B2C`,
            description:
              `Neue Webseitenstruktur und Hauptfokus auf Linkaufbau. 
              Als Resultat, Umsatz vervielfacht und nun grösster 
              Tabakwaren-Onlinehändler der Schweiz.`,
            image: `genussmittel`,
            linkTo: SCREENS.CONFIRM,
            dataKey: DATA.INSPIRED_BY,
            eventData: {
              action: 'Choice',
              label: 'Genussmittel',
            },
            animationTimeout: {
              enter: 1300,
              exit: 0,
            }
          },
          {
            title: `Informatik`,
            type: `Dienstleistungen B2B`,
            description:
              `Umsetzung von gezielten Massnahmen anhand 
              vordefinierten Kundenwünschen, zur Erhöhung von 
              Kundenbesuchen und -anfragen. Analytics-Einbindung 
              und stetige Performanceüberwachung.`,
            image: `informatik`,
            linkTo: SCREENS.CONFIRM,
            dataKey: DATA.INSPIRED_BY,
            eventData: {
              action: 'Choice',
              label: 'Informatik',
            },
            animationTimeout: {
              enter: 1600,
              exit: 0,
            }
          },
          {
            title: `Gesundheit`,
            type: `Dienstleistungen B2C`,
            description:
              `Umsetzung von gezielten Suchmaschinenoptimierung-Massnahmen 
              anhand eines Massnahmekataloges, der im Auftrag des Kunden 
              erstellt wurde. Analytics eingebettet um Performance zu messen.`,
            image: `gesundheit`,
            linkTo: SCREENS.CONFIRM,
            dataKey: DATA.INSPIRED_BY,
            eventData: {
              action: 'Choice',
              label: 'Gesundheit',
            },
            animationTimeout: {
              enter: 1900,
              exit: 0,
            }
          },
          {
            title: `Events / Unterhaltung`,
            type: `Dienstleistungen B2B`,
            description:
              `Unterstützung im Bereich Suchmaschinenoptimierung für die 
              Lancierung der neuen Webseite.  Implementierung aller 
              Analytic-Tools zur Messung der kanalübergreifenden Konversionen. 
              Aufbau von Google Ads, Facebook Ads und LinkedIn Ads Kampagnen.`,
            image: `events-unterhaltung`,
            linkTo: SCREENS.CONFIRM,
            dataKey: DATA.INSPIRED_BY,
            eventData: {
              action: 'Choice',
              label: 'Events / Unterhaltung',
            },
            animationTimeout: {
              enter: 2200,
              exit: 0,
            }
          },
          {
            title: `Personalvermittlung`,
            type: `Dienstleistungen B2C`,
            description:
              `Aufbau einer Landing-Page für ein Startup zur Validierung 
              einer Geschäftsidee. Implementierung aller Analytic-Tools 
              zur Messung der Konversionen, Aufbau von Google Ads und 
              Facebook Ads Kampagnen mit einer Konversionsrate von über 10%.`,
            image: `personalvermittlung`,
            linkTo: SCREENS.CONFIRM,
            dataKey: DATA.INSPIRED_BY,
            eventData: {
              action: 'Choice',
              label: 'Personalvermittlung',
            },
            animationTimeout: {
              enter: 2500,
              exit: 0,
            }
          }
        ]
      },
      {
        type: MSG_TYPES.LINK,
        text: `Zurück`,
        linkTo: SCREENS.CHOICE,
        animationTimeout: {
          enter: 2800,
          exit: 0,
        }
      },
    ]
  },
  [SCREENS.CONFIRM]: {
    messages:
      [
        {
          type: MSG_TYPES.DEFAULT,
          text: `Um dich zu kontaktieren benötigen wir noch einige Informationen.`,
          animationTimeout: {
            enter: 500,
            exit: 0,
          },
        },
        {
          type: MSG_TYPES.INPUT,
          dataKey: DATA.USER_PHONE,
          placeholder: '+41 044 585 19 11',
          animationTimeout: {
            enter: 900,
            exit: 0,
          },
        },
        {
          type: MSG_TYPES.INPUT,
          dataKey: DATA.USER_EMAIL,
          placeholder: 'max.muster@muster.ch',
          animationTimeout: {
            enter: 1200,
            exit: 0,
          },
        },
        {
          type: MSG_TYPES.ACTION,
          text: `Beratung abschliessen`,
          linkTo: SCREENS.BYE,
          eventData: {
            action: 'Lead',
          },
          animationTimeout: {
            enter: 1400,
            exit: 0,
          },
        },
        {
          type: MSG_TYPES.LINK,
          text: `Zurück`,
          linkTo: SCREENS.CHOICE,
          animationTimeout: {
            enter: 1500,
            exit: 0,
          },
        },
      ],
      validate({ [DATA.USER_PHONE]:userPhone, [DATA.USER_EMAIL]:email }) {
        const phoneNumberRegEx = /[0-9 ()-.]/;

        if (!userPhone.length || !phoneNumberRegEx.test(userPhone)) {
          return DATA.USER_PHONE;
        }

        const emailReqEx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@(([[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!email.length || !emailReqEx.test(email)) {
          return DATA.USER_EMAIL;
        }

        return '';
      }
  },
  [SCREENS.BYE]: {

    isBotHeadSmall: false,
    messages:
      [
        {
          type: MSG_TYPES.DEFAULT,
          text: `Vielen Dank für deine Anfrage!`,
          animationTimeout: {
            enter: 500,
            exit: 0,
          }
        },
        {
          type: MSG_TYPES.DEFAULT,
          text: `Wir werden dich in innerhalb von zwei Arbeitstagen kontaktieren.`,
          animationTimeout: {
            enter: 800,
            exit: 0,
          }
        },
        {
          type: MSG_TYPES.LINK,
          text: `Zurück zur Startseite`,
          linkTo: 'https://sixa.ch/',
          isExternal: true,
          animationTimeout: {
            enter: 1100,
            exit: 0,
          }
        },
      ],
  },
};

export const defaultStepName = () => SCREENS.WELCOME;
