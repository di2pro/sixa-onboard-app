import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { CSSTransition } from 'react-transition-group';

import botHead from '../../images/robo-head.svg'

export default class BotHead extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isMounted: false,
    };

    this.onEntered = this.onEntered.bind(this);
  }

  componentDidMount() {
    this.setState(() => ({
      isMounted: true,
    }));
  }

  onEntered() {
    const { startLoadingMessages } = this.props;

    setTimeout(startLoadingMessages, 1000);
  }

  render() {
    const { isMounted } = this.state;
    const { isBotHeadSmall } = this.props;

    return (
      <CSSTransition
        in={isMounted}
        classNames="xa_bot__avatar"
        timeout={{ enter: 400, exit: 400 }}
        onEntered={this.onEntered}
      >
        <div className={classNames('xa_bot__avatar', {'xa_bot__avatar--small': isBotHeadSmall})}>
          <img src={botHead} alt=""/>
        </div>
      </CSSTransition>
    );
  }
}

BotHead.propTypes = {
  startLoadingMessages: PropTypes.func.isRequired,
  isBotHeadSmall: PropTypes.bool,
};

BotHead.defaultProps = {
  isBotHeadSmall: true,
};
