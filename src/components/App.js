import React, { Component } from 'react';

import BotHead from './Bot/BotHead';
import Message from './BotMessages/Message';
import * as xaApp from '../state';

import logo from '../images/logo-sixa.svg';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: xaApp.data,
      steps: xaApp.steps,
      activeStepName: xaApp.defaultStepName(),
      isBotLoading: false,
    };

    this.goToStep = this.goToStep.bind(this);
    this.dispatch = this.dispatch.bind(this);
    this.sendGAEvent = this.sendGAEvent.bind(this);
    this.startLoadingMessages = this.startLoadingMessages.bind(this);
  }

  componentDidMount() {
    this.dataLayer = window.dataLayer || [];
  }

  completeLoading() {
    setTimeout(function() {
      this.setState((previousState) => ({
        ...previousState,
        isBotLoading: false,
      }))
    }.bind(this), 1600);
  }

  clearValidationError() {
    setTimeout(function () {
      this.setState(({ data, ...previousState}) => ({
        ...previousState,
        data: {
          ...data,
          [xaApp.DATA.VALIDATION_ERROR]: ''
        }
      }));
    }.bind(this), 1200);
  }

  startLoadingMessages() {
    this.setState(({ isBotLoading, ...previousState}) => ({
      ...previousState,
      isBotLoading: true,
    }));

    this.completeLoading();
  }

  sendDataAsEmail() {
    const { data } = this.state;

    fetch('api/email', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data),
    });

    this.setState(previousState => ({
      ...previousState,
      data: xaApp.data,
    }));
  }

  sendGAEvent({ action, label = '' }) {
    const { data } = this.state;
    const event = {
      'event': 'online-berater',
      'event-category': 'Online-berater',
      'event-action': action,
    };

    if (label) {
      event['event-label'] = data.hasOwnProperty(label)
        ? data[label]
        : label;
    }

    this.dataLayer.push(event);
  };

  dispatch(updatedData) {
    this.setState(({ data, ...previousState }) => ({
      ...previousState,
      data: {
        ...data,
        ...updatedData,
      }
    }));
  }

  goToStep(name, eventData, isBack = false) {
    if (isBack) {
      return this.goBackToStep(name);
    }

    const { activeStepName, data, steps } = this.state;

    if (steps[activeStepName].hasOwnProperty('validate')) {
      const error = steps[activeStepName].validate(data);

      if (error) {
        this.setState(({ data, ...previousState }) => ({
          ...previousState,
          data: {
            ...data,
            [xaApp.DATA.VALIDATION_ERROR]: error,
          }
        }));

        return this.clearValidationError();
      }
    }

    if (name === xaApp.SCREENS.BYE) {
      this.sendDataAsEmail();
    }

    this.setState((previousState) => ({
      ...previousState,
      activeStepName: name,
      isBotLoading: true,
    }));

    this.sendGAEvent(eventData);
    this.completeLoading();
  }

  goBackToStep(name) {
    const { data, activeStepName } = this.state;
    const updatedState = {
      activeStepName: name,
      isBotLoading: true,
    };

    if (name === xaApp.SCREENS.CHOICE) {
      if (data[xaApp.DATA.INSPIRED_BY]) {
        updatedState.activeStepName = activeStepName === xaApp.SCREENS.INSPIRATION
          ? name : xaApp.SCREENS.INSPIRATION;
        updatedState.data = {
          ...data,
          [xaApp.DATA.INSPIRED_BY]: '',
        };
      }

      if (data[xaApp.DATA.LAST_PICKED]) {
        updatedState.activeStepName = activeStepName === xaApp.SCREENS.PICKER
          ? name : xaApp.SCREENS.PICKER;
        updatedState.data = {
          ...data,
          [xaApp.DATA.LAST_PICKED]: '',
        };
      }
    }

    this.setState((previousState) => ({
      ...previousState,
      ...updatedState
    }));

    this.completeLoading();
    this.clearValidationError();
  }

  render() {
    const { steps, activeStepName, data, isBotLoading } = this.state;
    const { isBotHeadSmall, messages } = steps[activeStepName];

    return (
      <section className="xa_app__container">
        <header className="xa_app__header">
          <a href="https://sixa.ch">
            <img className="xa_app__logo" src={logo} alt="Sixa" />
          </a>
        </header>
        <main className="xa_app__content">
          <BotHead
            isBotHeadSmall={isBotHeadSmall}
            startLoadingMessages={this.startLoadingMessages}
          />

          {isBotLoading
            ? <div className="spinner">
                <div className="dot1" />
                <div className="dot2" />
              </div>
            : null}

          {messages.map(({ type, ...message }, index) => (
              <Message
                key={index}
                type={type}
                dispatch={this.dispatch}
                goToStep={this.goToStep}
                isFirst={index === 0}
                appData={data}
                isBotLoading={isBotLoading}
                {...message}
              />
          ))}
        </main>
      </section>
    );
  }
}
