import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { CSSTransition } from 'react-transition-group';

import { DATA } from '../../state';

export default class Input extends Component{
  constructor(props) {
    super(props);

    this.input = React.createRef();
    this.onChange = this.onChange.bind(this);
  }

  onChange() {
    const { dispatch, dataKey } = this.props;

    dispatch({
      [dataKey]: this.input.current.value,
    });
  }

  render() {
    const { placeholder, appData, dataKey, isBotLoading, animationTimeout } = this.props;

    return (
      <CSSTransition
        in={!isBotLoading}
        classNames="xa_bot__message"
        timeout={animationTimeout}
      >
        <div className="xa_bot__message xa_bot__message--input">
          <input
            ref={this.input}
            onChange={this.onChange}
            placeholder={placeholder}
            value={appData[dataKey]}
            className={classNames({
              'invalid': appData[DATA.VALIDATION_ERROR] === dataKey
            })}
          />
        </div>
      </CSSTransition>
    );
  }
}

Input.propTypes = {
  dispatch: PropTypes.func.isRequired,
  appData: PropTypes.object.isRequired,
  dataKey: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  isBotLoading: PropTypes.bool,
  animationTimeout: PropTypes.object,
};

Input.defaultProps = {
  isBotLoading: false,
  animationTimeout: {
    enter: 1000,
    exit: 0,
  }
};
