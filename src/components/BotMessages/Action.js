import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { CSSTransition } from 'react-transition-group';

export default function Action({
  text,
  linkTo,
  goToStep,
  isTransparent,
  eventData,
  animationTimeout,
  isBotLoading,
}) {
  const onClick = event => {
    event.preventDefault();

    goToStep(linkTo, eventData);
  };

  return (
    <CSSTransition
      in={!isBotLoading}
      classNames="xa_bot__action"
      timeout={animationTimeout}
    >
      <button className={
        classNames('xa_bot__action', {'xa_bot__action--transparent': isTransparent})
      } onClick={onClick}>
        <span>{text}</span>
      </button>
    </CSSTransition>
  );
}

Action.propTypes = {
  text: PropTypes.string.isRequired,
  linkTo: PropTypes.string.isRequired,
  goToStep: PropTypes.func.isRequired,
  eventData: PropTypes.object.isRequired,
  isTransparent: PropTypes.bool,
  isBotLoading: PropTypes.bool,
  animationTimeout: PropTypes.object,
};

Action.defaultProps = {
  isTransparent: false,
  isBotLoading: false,
  animationTimeout: {
    enter: 1000,
    exit: 0,
  }
};
