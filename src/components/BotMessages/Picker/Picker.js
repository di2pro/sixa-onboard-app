import React from 'react';
import PropTypes from 'prop-types';

import Accordion from './Accordion';

export default function Picker({ items, ...props }) {
    return (
      items.map((item, index) => (
        <Accordion key={index} {...props} {...item} />
      ))
    );
}

Picker.propTypes = {
  appData: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired,
  isBotLoading: PropTypes.bool,
};

Picker.defaultProps = {
  isBotLoading: false,
};
