import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { CSSTransition } from 'react-transition-group';

import CheckBox from './CheckBox';
import { DATA } from '../../../state';

import chevron from '../../../images/chevron-down-light.svg';

export default class Accordion extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isCollapsed: true
    };

    this.toggleAccordion = this.toggleAccordion.bind(this);
  }

  toggleAccordion(event) {
    event.preventDefault();

    const { isCollapsed } = this.state;
    const { dispatch, title } = this.props;

    if (isCollapsed) {
      dispatch({
        [DATA.LAST_PICKED]: title,
      });
    }

    this.setState(({ isCollapsed }) => ({ isCollapsed: !isCollapsed }));
  }

  render() {
    const { title, description, options, appData, dispatch, isBotLoading, animationTimeout } = this.props;
    const { isCollapsed } = this.state;

    return (
      <CSSTransition
        in={!isBotLoading}
        classNames="xa_accordion"
        timeout={animationTimeout}
      >
        <div className="xa_accordion">
          <div className="xa_accordion__head" onClick={this.toggleAccordion}>
            <h2 className="xa_accordion__header">{title}</h2>
            <h5 className="xa_accordion__sub-header">{description}</h5>
            <div className={classNames('xa_accordion__chevron', {'xa_accordion__chevron--point-to-left': isCollapsed })}>
              <img src={chevron} alt="" />
            </div>
          </div>
          <div className={classNames('xa_accordion__content', {
            'xa_accordion__content--collapsed': isCollapsed
          })}>
            {options.map((option, index) => (
              <CheckBox
                key={index}
                appData={appData}
                index={index}
                title={title}
                dispatch={dispatch}
                {...option} />
            ))}
          </div>
        </div>
      </CSSTransition>
    );
  }
}

Accordion.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  appData: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  animationTimeout: PropTypes.object.isRequired,
  isBotLoading: PropTypes.bool.isRequired,
};
