import React, { Component } from 'react';
import PropTypes from 'prop-types';

import checkMark from '../../../images/check-light.svg';

export default class CheckBox extends Component {
  constructor(props) {
    super(props);

    this.onChange = this.onChange.bind(this);
  }

  onChange() {
    const { dispatch, dataKey, appData } = this.props;

    dispatch({
      [dataKey]: !appData[dataKey],
    })
  }

  render() {
    const { title, index, label, appData, dataKey } = this.props;

    return (
      <label htmlFor={`select-item-${title}-${index}`} className="xa_select">
        <input
          id={`select-item-${title}-${index}`}
          type="checkbox"
          onChange={this.onChange}
          value={appData[dataKey]}
        />
        <div className="xa_select__checkbox">
          <img src={checkMark} alt="" />
        </div>
        {label}
      </label>
    );
  }
}

CheckBox.propTypes = {
  title: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  label: PropTypes.string.isRequired,
  dataKey: PropTypes.string.isRequired,
  appData: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
};
