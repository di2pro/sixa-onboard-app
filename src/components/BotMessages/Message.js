import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { CSSTransition } from 'react-transition-group';

import Action from './Action';
import Input from './Input';
import Picker from './Picker/Picker';
import InspirationCards from './InspirationCards/InspirationCards';
import Link from './Link';

import { MSG_TYPES } from '../../state';

export default function Message({ type, ...props}) {
  switch (type) {
    case MSG_TYPES.INSPIRATION:
      return <InspirationCards {...props} />;

    case MSG_TYPES.PICKER:
      return <Picker {...props} />;

    case MSG_TYPES.ACTION_TRANSPARENT:
      return <Action {...props} isTransparent={true} />;

    case MSG_TYPES.ACTION:
      return <Action {...props} />;

    case MSG_TYPES.INPUT:
      return <Input {...props} />;

    case MSG_TYPES.LINK:
      return <Link {...props} />;

    default:
      const { appData, text, isFirst, isBotLoading, animationTimeout } = props;

      return (
        <CSSTransition
          in={!isBotLoading}
          classNames="xa_bot__message"
          timeout={animationTimeout}
        >
          <div className={
            classNames('xa_bot__message', {'xa_bot__message--first': isFirst}
            )}>
            <span>{text.replace(/%(.*)%/, (match, dataKey) => appData[dataKey])}</span>
          </div>
        </CSSTransition>
      );
  }
};

Message.propTypes = {
  type: PropTypes.string.isRequired,
  appData: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  goToStep: PropTypes.func.isRequired,
  isFirst: PropTypes.bool,
  isBotLoading: PropTypes.bool,
  animationTimeout: PropTypes.object,
};

Message.defaultProps = {
  isFirst: false,
  isBotLoading: false,
  animationTimeout: {
    enter: 1000,
    exit: 0,
  }
};
