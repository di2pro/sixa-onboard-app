import React from 'react';
import PropTypes from 'prop-types';
import Card from './Card';


export default function InspirationCards({ cards, ...props }) {
  return (
    <div className="xa_picker">
      {cards.map((card, index) => (
        <Card key={index} {...props} {...card} />
      ))}
    </div>
  );
}

InspirationCards.propTypes = {
  cards: PropTypes.array.isRequired,
  appData: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  goToStep: PropTypes.func.isRequired,
  isBotLoading: PropTypes.bool,
};

InspirationCards.defaultProps = {
  isBotLoading: false,
};
