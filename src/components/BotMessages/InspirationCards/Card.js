import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';

export default function Card({
    title,
    type,
    description,
    goToStep,
    appData,
    dispatch,
    image,
    dataKey,
    linkTo,
    eventData,
    isBotLoading,
    animationTimeout,
  }) {
  const onClick = () => {
    dispatch({
      [dataKey]: title,
    });

    goToStep(linkTo, eventData);
  };

  return (
    <CSSTransition
      in={!isBotLoading}
      classNames="xa_picker__card"
      timeout={animationTimeout}
    >
      <div className={`xa_picker__card xa_picker__card--${image}`} onClick={onClick}>
        <div className="xa_picker__content">
          <h2 className="xa_picker__header">{title}</h2>
          <h5 className="xa_picker__sub-header">{type}</h5>
          <p className="xa_picker__description">{description}</p>
        </div>
        <div className="xa_picker__overlay" />
      </div>
    </CSSTransition>
  );
}

Card.propTypes = {
  appData: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  goToStep: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  dataKey: PropTypes.string.isRequired,
  linkTo: PropTypes.string.isRequired,
  eventData: PropTypes.object.isRequired,
  isBotLoading: PropTypes.bool.isRequired,
  animationTimeout: PropTypes.object.isRequired,
};
