import React from 'react';
import PropTypes from 'prop-types';
import { CSSTransition } from 'react-transition-group';

export default function Link({
  text, linkTo, isExternal, goToStep, isBotLoading, animationTimeout
}) {
  const onClick = event => {
    event.preventDefault();

    goToStep(linkTo, null, true)
  };

  return (
    <CSSTransition
      in={!isBotLoading}
      classNames="xa_bot__link"
      timeout={animationTimeout}
    >
      {isExternal
        ? <a className="xa_bot__link" href={linkTo}>
          <span>{text}</span>
        </a>
        : <a className="xa_bot__link" onClick={onClick} href="">
          <span>{text}</span>
        </a>}
    </CSSTransition>
  );
}

Link.propTypes = {
  text: PropTypes.string.isRequired,
  linkTo: PropTypes.string.isRequired,
  goToStep: PropTypes.func.isRequired,
  isExternal: PropTypes.bool,
  isBotLoading: PropTypes.bool,
  animationTimeout: PropTypes.object,

};

Link.defaultProps = {
  isExternal: false,
  isBotLoading: false,
  animationTimeout: {
    enter: 1000,
    exit: 0,
  }
};
