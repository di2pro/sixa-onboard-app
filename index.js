if (typeof(PhusionPassenger) !== 'undefined') {
  PhusionPassenger.configure({ autoInstall: false });
}

const express = require('express');
const bodyParser = require('body-parser');
const nodeMailer = require('nodemailer');

const port = process.env.PORT || 8080;
const DATA = {
  USER_NAME: 'user-name',
  WEB_SITE: 'web-site',
  USER_EMAIL: 'e-mail',
  USER_PHONE: 'phone',
  SEO_1: 'seo-1',
  SEO_2: 'seo-2',
  SEO_3: 'seo-3',
  SEO_4: 'seo-4',

  SEA_1: 'sea-1',
  SEA_2: 'sea-2',
  SEA_3: 'sea-3',

  ANALYTICS_1: 'eventData-1',
  ANALYTICS_2: 'eventData-2',
  ANALYTICS_3: 'eventData-3',
  ANALYTICS_4: 'eventData-4',

  SMM_1: 'smm-1',
  SMM_2: 'smm-2',
  SMM_3: 'smm-3',

  CRO_1: 'cro-1',
  CRO_2: 'cro-2',
  CRO_3: 'cro-3',
  CRO_4: 'cro-4',

  WEB_DEV_1: 'web-dev-1',
  WEB_DEV_2: 'web-dev-2',

  INSPIRED_BY: 'inspired-by',
  LAST_PICKED: 'last-picked',
  VALIDATION_ERROR: 'validation-error',
};
const transporter = nodeMailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'ian.schaefer@sixa.ch',
    pass: 'qnaznnaqpyjzowkf',
  }
});
const mailOptions = {
  from: 'ian.schaefer@sixa.ch',
  to: 'info@sixa.ch',
  subject: 'sixa Online Berater',
};
const app = express();

app.use(bodyParser.json());

app.post('/online-berater/api/email', (req, res) => {
  const { body } = req;

  let emailBody = ``;

  if (body[DATA.INSPIRED_BY]) {
    emailBody = `<p>User has been inspired by ${body[DATA.INSPIRED_BY]}</p>`;
  } else {
    emailBody =
      `
      <h4>Suchmaschinenoptimierung</h4>
        <p>${body[DATA.SEO_1] ? 'Ich möchte meine Rankings verbessern' : ''}</p>
        <p>${body[DATA.SEO_2] ? 'Ich möchte einen SEO-Check durchführen' : ''}</p>
        <p>${body[DATA.SEO_3] ? 'Ich möchte mehr Kunden über Google erhalten (organisch)' : ''}</p>
        <p>${body[DATA.SEO_4] ? 'Ich möchte mein Linkprofil verbessern' : ''}</p>
      <p>&nbsp;</p>
      <h4>Suchmaschinenwerbung</h4>
        <p>${body[DATA.SEA_1] ? 'Ich möchte mehr Kunden über Google Ads erhalten' : ''}</p>
        <p>${body[DATA.SEA_2] ? 'Ich möchte meine Google Ads Kampagne nicht mehr selbst verwalten' : ''}</p>
        <p>${body[DATA.SEA_3] ? 'Ich möchte die Performance meiner Google Ads Kampagne verbessern' : ''}</p>
      <p>&nbsp;</p>
      <h4>Analytics</h4>
        <p>${body[DATA.ANALYTICS_1] ? 'Ich möchte alle Metriken meiner Webseite erfassen' : ''}</p>
        <p>${body[DATA.ANALYTICS_2] ? 'Ich möchte die Konversionsrate meiner Webseite messen' : ''}</p>
        <p>${body[DATA.ANALYTICS_3] ? 'Ich möchte wissen, wo ich Kunden verliere' : ''}</p>
        <p>${body[DATA.ANALYTICS_4] ? 'Ich möchte wissen, wo sich Kunden aufhalten' : ''}</p>
      <p>&nbsp;</p>
      <h4>Konversionsoptimierung</h4>
        <p>${body[DATA.CRO_1] ? 'Ich möchte die Performance meiner Webseite verbessern' : ''}</p>
        <p>${body[DATA.CRO_2] ? 'Ich möchte meine Konversionsrate erhöhen' : ''}</p>
        <p>${body[DATA.CRO_3] ? 'Ich möchte meine Absprungrate verringern' : ''}</p>
        <p>${body[DATA.CRO_4] ? 'Ich möchte meine Sitzungsdauer verlängern' : ''}</p>
      <p>&nbsp;</p>
      <h4>Social Media Marketing</h4>
        <p>${body[DATA.SMM_1] ? 'Ich möchte mehr Kunden über Facebook, Twitter oder LinkedIn erhalten' : ''}</p>
        <p>${body[DATA.SMM_2] ? 'Ich möchte meine Facebook, Twitter oder LinkedIn Ads Kampagne nicht mehr selbst verwalten' : ''}</p>
        <p>${body[DATA.SMM_3] ? 'Ich möchte die Performance meiner Facebook, Twitter oder LinkedIn Ads Kampagne Kampagne verbessern' : ''}</p>
      <p>&nbsp;</p>
      <h4>Web Development</h4>
        <p>${body[DATA.WEB_DEV_1] ? 'Ich möchte eine neue Webseite' : ''}</p>
        <p>${body[DATA.WEB_DEV_2] ? 'Ich möchte meine Webseite überarbeiten' : ''}</p>
      <p>&nbsp;</p>
    `;
  }

  mailOptions.html =
    `
      <!doctype html>
      <html>
        <head>
          <meta name="viewport" content="width=device-width" />
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
          <title>sixa Online Berater</title>
          <style>
            /* -------------------------------------
                GLOBAL RESETS
            ------------------------------------- */
            img {
              border: none;
              -ms-interpolation-mode: bicubic;
              max-width: 100%; }
      
            body {
              background-color: #f6f6f6;
              font-family: sans-serif;
              -webkit-font-smoothing: antialiased;
              font-size: 14px;
              line-height: 1.4;
              margin: 0;
              padding: 0;
              -ms-text-size-adjust: 100%;
              -webkit-text-size-adjust: 100%; }
      
            table {
              border-collapse: separate;
              mso-table-lspace: 0pt;
              mso-table-rspace: 0pt;
              width: 100%; }
              table td {
                font-family: sans-serif;
                font-size: 14px;
                vertical-align: top; }
      
            /* -------------------------------------
                BODY & CONTAINER
            ------------------------------------- */
      
            .body {
              background-color: #f6f6f6;
              width: 100%; }
      
            /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
            .container {
              display: block;
              Margin: 0 auto !important;
              /* makes it centered */
              max-width: 580px;
              padding: 10px;
              width: 580px; }
      
            /* This should also be a block element, so that it will fill 100% of the .container */
            .content {
              box-sizing: border-box;
              display: block;
              Margin: 0 auto;
              max-width: 580px;
              padding: 10px; }
      
            /* -------------------------------------
                HEADER, FOOTER, MAIN
            ------------------------------------- */
            .main {
              background: #ffffff;
              border-radius: 3px;
              width: 100%; }
      
            .wrapper {
              box-sizing: border-box;
              padding: 20px; }
      
            .content-block {
              padding-bottom: 10px;
              padding-top: 10px;
            }
      
            .footer {
              clear: both;
              Margin-top: 10px;
              text-align: center;
              width: 100%; }
              .footer td,
              .footer p,
              .footer span,
              .footer a {
                color: #999999;
                font-size: 12px;
                text-align: center; }
      
            /* -------------------------------------
                TYPOGRAPHY
            ------------------------------------- */
            h1,
            h2,
            h3,
            h4 {
              color: #000000;
              font-family: sans-serif;
              font-weight: 400;
              line-height: 1.4;
              margin: 0;
              Margin-bottom: 30px; }
      
            h1 {
              font-size: 35px;
              font-weight: 300;
              text-align: center;
              text-transform: capitalize; }
      
            p,
            ul,
            ol {
              font-family: sans-serif;
              font-size: 14px;
              font-weight: normal;
              margin: 0;
              Margin-bottom: 15px; }
              p li,
              ul li,
              ol li {
                list-style-position: inside;
                margin-left: 5px; }
      
            a {
              color: #3498db;
              text-decoration: underline; }
      
            /* -------------------------------------
                BUTTONS
            ------------------------------------- */
            .btn {
              box-sizing: border-box;
              width: 100%; }
              .btn > tbody > tr > td {
                padding-bottom: 15px; }
              .btn table {
                width: auto; }
              .btn table td {
                background-color: #ffffff;
                border-radius: 5px;
                text-align: center; }
              .btn a {
                background-color: #ffffff;
                border: solid 1px #3498db;
                border-radius: 5px;
                box-sizing: border-box;
                color: #3498db;
                cursor: pointer;
                display: inline-block;
                font-size: 14px;
                font-weight: bold;
                margin: 0;
                padding: 12px 25px;
                text-decoration: none;
                text-transform: capitalize; }
      
            .btn-primary table td {
              background-color: #3498db; }
      
            .btn-primary a {
              background-color: #3498db;
              border-color: #3498db;
              color: #ffffff; }
      
            /* -------------------------------------
                OTHER STYLES THAT MIGHT BE USEFUL
            ------------------------------------- */
            .last {
              margin-bottom: 0; }
      
            .first {
              margin-top: 0; }
      
            .align-center {
              text-align: center; }
      
            .align-right {
              text-align: right; }
      
            .align-left {
              text-align: left; }
      
            .clear {
              clear: both; }
      
            .mt0 {
              margin-top: 0; }
      
            .mb0 {
              margin-bottom: 0; }
      
            .preheader {
              color: transparent;
              display: none;
              height: 0;
              max-height: 0;
              max-width: 0;
              opacity: 0;
              overflow: hidden;
              mso-hide: all;
              visibility: hidden;
              width: 0; }
      
            .powered-by a {
              text-decoration: none; }
      
            hr {
              border: 0;
              border-bottom: 1px solid #f6f6f6;
              Margin: 20px 0; }
      
            /* -------------------------------------
                RESPONSIVE AND MOBILE FRIENDLY STYLES
            ------------------------------------- */
            @media only screen and (max-width: 620px) {
              table[class=body] h1 {
                font-size: 28px !important;
                margin-bottom: 10px !important; }
              table[class=body] p,
              table[class=body] ul,
              table[class=body] ol,
              table[class=body] td,
              table[class=body] span,
              table[class=body] a {
                font-size: 16px !important; }
              table[class=body] .wrapper,
              table[class=body] .article {
                padding: 10px !important; }
              table[class=body] .content {
                padding: 0 !important; }
              table[class=body] .container {
                padding: 0 !important;
                width: 100% !important; }
              table[class=body] .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important; }
              table[class=body] .btn table {
                width: 100% !important; }
              table[class=body] .btn a {
                width: 100% !important; }
              table[class=body] .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important; }}
      
            /* -------------------------------------
                PRESERVE THESE STYLES IN THE HEAD
            ------------------------------------- */
            @media all {
              .ExternalClass {
                width: 100%; }
              .ExternalClass,
              .ExternalClass p,
              .ExternalClass span,
              .ExternalClass font,
              .ExternalClass td,
              .ExternalClass div {
                line-height: 100%; }
              .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important; }
              .btn-primary table td:hover {
                background-color: #34495e !important; }
              .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important; } }
      
          </style>
        </head>
        <body class="">
          <table border="0" cellpadding="0" cellspacing="0" class="body">
            <tr>
              <td>&nbsp;</td>
              <td class="container">
                <div class="content">
                  <table class="main">
      
                    <!-- START MAIN CONTENT AREA -->
                    <tr>
                      <td class="wrapper">
                        <table border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td>
                              <p>FullName: ${body[DATA.USER_NAME]}</p>
                              <p>Web-site: ${body[DATA.WEB_SITE]}</p>
                              <p>Telefon: ${body[DATA.USER_PHONE]}</p>
                              <p>E-mail: ${body[DATA.USER_EMAIL]}</p>
                              <p>&nbsp;</p>
                              ${emailBody}
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
      
                  <!-- END MAIN CONTENT AREA -->
                  </table>
      
                  <!-- START FOOTER -->
                  <div class="footer">
                    <table border="0" cellpadding="0" cellspacing="0">
      
                      <tr>
                        <td class="content-block powered-by">
                          Powered by <a href="http://sixa.sh">Sixa</a>.
                        </td>
                      </tr>
                    </table>
                  </div>
                  <!-- END FOOTER -->
      
                <!-- END CENTERED WHITE CONTAINER -->
                </div>
              </td>
              <td>&nbsp;</td>
            </tr>
          </table>
        </body>
      </html>
    `;

  transporter.sendMail(mailOptions);

  return res.status(200).json({});
});

if (typeof(PhusionPassenger) !== 'undefined') {
  app.listen('passenger');
} else {
  app.listen(port);
}
